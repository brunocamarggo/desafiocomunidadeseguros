package com.brcamba.desafiocomunidadeseguros.usecase.gateway

import com.brcamba.desafiocomunidadeseguros.usecase.domain.StatisticsDomain
import com.brcamba.desafiocomunidadeseguros.usecase.domain.TransactionDomain
import java.time.LocalDateTime

interface TransactionGateway {

    fun save(transactionDomain: TransactionDomain)

    fun getTransactionsByTimeAgo(time: LocalDateTime): List<TransactionDomain>

    fun getStatistics(time: LocalDateTime): StatisticsDomain

    fun deleteAll()

}