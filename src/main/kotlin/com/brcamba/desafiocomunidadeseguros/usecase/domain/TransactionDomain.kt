package com.brcamba.desafiocomunidadeseguros.usecase.domain

import java.math.BigDecimal
import java.time.LocalDateTime

class TransactionDomain (
        val value: BigDecimal,
        val date: LocalDateTime
)