package com.brcamba.desafiocomunidadeseguros.usecase.domain

import java.math.BigDecimal

class StatisticsDomain(
        val count: Long,
        val sum: Double,
        val average: Double,
        val min: Double,
        val max: Double
)

