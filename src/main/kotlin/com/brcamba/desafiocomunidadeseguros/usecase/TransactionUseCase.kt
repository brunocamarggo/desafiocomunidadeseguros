package com.brcamba.desafiocomunidadeseguros.usecase

import com.brcamba.desafiocomunidadeseguros.usecase.domain.StatisticsDomain
import com.brcamba.desafiocomunidadeseguros.usecase.domain.TransactionDomain
import com.brcamba.desafiocomunidadeseguros.usecase.gateway.TransactionGateway
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import java.time.LocalDateTime

@Component
class TransactionUseCase(@Autowired val transactionGateway: TransactionGateway) {

    fun saveTransaction(transactionDomain: TransactionDomain) {
        transactionGateway.save(transactionDomain)
    }

    fun getStatisticsAMinuteAgo(): StatisticsDomain {
        val minuteAgo = LocalDateTime.now().minusMinutes(1)
        return transactionGateway.getStatistics(minuteAgo)
    }

    fun deleteAllTransactions() {
        transactionGateway.deleteAll()
    }
}