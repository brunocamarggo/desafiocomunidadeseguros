package com.brcamba.desafiocomunidadeseguros.entrypoint.exception

import com.brcamba.desafiocomunidadeseguros.entrypoint.model.response.ErrorResponse
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.http.converter.HttpMessageNotReadableException
import org.springframework.web.bind.MethodArgumentNotValidException
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler


@ControllerAdvice
class ExceptionHandlerController {

    @ExceptionHandler(HttpMessageNotReadableException::class)
    fun httpMessageNotReadableExceptionHandler(httpMessageNotReadableException: HttpMessageNotReadableException): ResponseEntity<String> {
        return ResponseEntity(HttpStatus.UNPROCESSABLE_ENTITY)
    }

    @ExceptionHandler(MethodArgumentNotValidException::class)
    fun methodArgumentNotValidExceptionHandler(methodArgumentNotValidException: MethodArgumentNotValidException): ResponseEntity<ErrorResponse> {
        val errorMensage = "The value '${methodArgumentNotValidException.bindingResult.fieldError?.field}' ${methodArgumentNotValidException.bindingResult.fieldError?.defaultMessage}"
        return ResponseEntity(ErrorResponse(errorMensage), HttpStatus.BAD_REQUEST)
    }
}