package com.brcamba.desafiocomunidadeseguros.entrypoint.mapper

import com.brcamba.desafiocomunidadeseguros.entrypoint.model.request.TransactionRequest
import com.brcamba.desafiocomunidadeseguros.usecase.domain.TransactionDomain

class TransactionMapper {

    companion object {

        fun toDomain(transactionRequest: TransactionRequest): TransactionDomain {
            return TransactionDomain(value = transactionRequest.valor, date = transactionRequest.dataHora)
        }
    }
}