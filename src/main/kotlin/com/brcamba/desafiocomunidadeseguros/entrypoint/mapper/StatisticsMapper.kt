package com.brcamba.desafiocomunidadeseguros.entrypoint.mapper

import com.brcamba.desafiocomunidadeseguros.usecase.domain.StatisticsDomain
import java.util.*

class StatisticsMapper {

    companion object {
        fun toDomain(doubleSummaryStatistics: DoubleSummaryStatistics): StatisticsDomain {
            return StatisticsDomain(
                    count = doubleSummaryStatistics.count,
                    sum = doubleSummaryStatistics.sum,
                    average = doubleSummaryStatistics.average,
                    min = doubleSummaryStatistics.min,
                    max = doubleSummaryStatistics.max)
        }
    }
}