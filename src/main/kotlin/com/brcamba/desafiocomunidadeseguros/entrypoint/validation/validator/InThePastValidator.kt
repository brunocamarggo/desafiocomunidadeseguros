package com.brcamba.desafiocomunidadeseguros.entrypoint.validation.validator

import com.brcamba.desafiocomunidadeseguros.entrypoint.validation.InThePast
import java.time.LocalDateTime
import javax.validation.ConstraintValidator
import javax.validation.ConstraintValidatorContext

class InThePastValidator : ConstraintValidator<InThePast, LocalDateTime> {

    override fun isValid(date: LocalDateTime?, context: ConstraintValidatorContext?): Boolean {
        return date?.isBefore(LocalDateTime.now()) ?: false
    }

}