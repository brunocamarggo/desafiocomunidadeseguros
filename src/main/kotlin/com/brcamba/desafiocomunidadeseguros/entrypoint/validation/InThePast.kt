package com.brcamba.desafiocomunidadeseguros.entrypoint.validation

import com.brcamba.desafiocomunidadeseguros.entrypoint.validation.validator.InThePastValidator
import javax.validation.Constraint
import javax.validation.Payload
import kotlin.reflect.KClass

@Target(AnnotationTarget.FIELD)
@Retention(AnnotationRetention.RUNTIME)
@Constraint(validatedBy = [InThePastValidator::class])
@MustBeDocumented
annotation  class InThePast (
    val message: String = "must be a date in the past.",
    val groups: Array<KClass<Any>> = [],
    val payload: Array<KClass<Payload>> = []
)