package com.brcamba.desafiocomunidadeseguros.entrypoint.controller

import com.brcamba.desafiocomunidadeseguros.entrypoint.mapper.TransactionMapper
import com.brcamba.desafiocomunidadeseguros.entrypoint.model.request.TransactionRequest
import com.brcamba.desafiocomunidadeseguros.entrypoint.model.response.DataResponse
import com.brcamba.desafiocomunidadeseguros.usecase.TransactionUseCase
import com.brcamba.desafiocomunidadeseguros.usecase.domain.StatisticsDomain
import com.brcamba.desafiocomunidadeseguros.usecase.domain.TransactionDomain
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@RestController
class StatisticsController(@Autowired val transactionUseCase: TransactionUseCase) {

    @PostMapping("/transacao")
    @ResponseStatus(HttpStatus.CREATED)
    fun registerTransaction(@Valid @RequestBody transactionRequest: TransactionRequest) {
        transactionUseCase.saveTransaction(TransactionMapper.toDomain(transactionRequest))
    }

    @GetMapping("/estatistica")
    @ResponseStatus(HttpStatus.OK)
    fun getStatisticsAMinuteAgo(): DataResponse<StatisticsDomain> {
        return DataResponse(transactionUseCase.getStatisticsAMinuteAgo())
    }

    @DeleteMapping("/transacao")
    @ResponseStatus(HttpStatus.OK)
    fun deleteAllTransactions(): DataResponse<String> {
        transactionUseCase.deleteAllTransactions()
        return DataResponse("All transactions were deleted")
    }
}