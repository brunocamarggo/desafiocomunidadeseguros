package com.brcamba.desafiocomunidadeseguros.entrypoint.model.request

import com.brcamba.desafiocomunidadeseguros.entrypoint.validation.InThePast
import org.jetbrains.annotations.NotNull
import org.springframework.format.annotation.DateTimeFormat
import java.math.BigDecimal
import java.time.LocalDateTime
import javax.validation.constraints.Min

data class TransactionRequest(

        @field:Min(0)
        val valor: BigDecimal,

        @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
        @NotNull
        @InThePast
        val dataHora: LocalDateTime
)