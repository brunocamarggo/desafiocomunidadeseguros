package com.brcamba.desafiocomunidadeseguros.entrypoint.model.response

class DataResponse<T>(val data: T)