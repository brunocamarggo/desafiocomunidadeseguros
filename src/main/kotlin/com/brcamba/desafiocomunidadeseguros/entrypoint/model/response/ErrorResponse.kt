package com.brcamba.desafiocomunidadeseguros.entrypoint.model.response

import com.fasterxml.jackson.annotation.JsonProperty

class ErrorResponse(
        @JsonProperty("error_message")
        val errorMessage: String
)