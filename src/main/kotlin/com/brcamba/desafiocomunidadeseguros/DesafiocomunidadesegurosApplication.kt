package com.brcamba.desafiocomunidadeseguros

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class DesafiocomunidadesegurosApplication

fun main(args: Array<String>) {
	runApplication<DesafiocomunidadesegurosApplication>(*args)
}
