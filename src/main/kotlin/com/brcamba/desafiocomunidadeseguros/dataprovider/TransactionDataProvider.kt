package com.brcamba.desafiocomunidadeseguros.dataprovider

import com.brcamba.desafiocomunidadeseguros.entrypoint.mapper.StatisticsMapper
import com.brcamba.desafiocomunidadeseguros.usecase.domain.StatisticsDomain
import com.brcamba.desafiocomunidadeseguros.usecase.domain.TransactionDomain
import com.brcamba.desafiocomunidadeseguros.usecase.gateway.TransactionGateway
import org.springframework.stereotype.Component
import java.time.LocalDateTime
import java.util.*
import java.util.concurrent.CopyOnWriteArrayList

@Component
class TransactionDataProvider : TransactionGateway {

    var transactions =  Collections.synchronizedList(mutableListOf<TransactionDomain>())

    override fun save(transactionDomain: TransactionDomain) {
        transactions.add(transactionDomain)
    }

    override fun getTransactionsByTimeAgo(time: LocalDateTime): List<TransactionDomain> {
        return CopyOnWriteArrayList(transactions)
                .filter { transactionDomain -> transactionDomain.date.isAfter(time) }
    }

    override fun getStatistics(time: LocalDateTime): StatisticsDomain {
        val summaryStatistics = DoubleSummaryStatistics()

        getTransactionsByTimeAgo(time)
                .forEach { transaction -> summaryStatistics.accept(transaction.value.toDouble())}

        return StatisticsMapper.toDomain(summaryStatistics)
    }

    override fun deleteAll() {
        transactions =   Collections.synchronizedList(mutableListOf())
    }


}