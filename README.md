# DesafioComunidadeSeguros

**Desafio comunidade de Seguros**

Este é um desafio bacana tanto de desenvolvimento de software quanto de engenharia de software.

**Introdução**

Sua missão, caso você aceite, é criar uma API REST que recebe Transações e dá estatísticas sob essas transações. Para este desafio, a API deve ser criada utilizando-se de Java ou Kotlin e Spring Boot. Um bom lugar para se começar é o Spring Starter.

Este exercício foi projetado para usar bastante funcionalidades do Spring Boot e testar seus conhecimentos tanto em como montar e organizar o código da sua aplicação quanto seus conhecimentos em tópicos mais avançados como Thread Safety.

**Definição do exercício**

Você deve criar uma API REST com os seguintes endpoints:

1. POST /transacao

    Este é o endpoint que irá receber as Transações. Cada transação consiste de um valor e uma dataHora de quando ela aconteceu:

        {
           "valor": 123.45,
           "dataHora": "2020-08-07T10:11:12.000Z"
        }

    Os campos no JSON acima significam o seguinte:

        | Campo     | Significado                                           | Obrigatório?
        | ------    | -----------                                           | Sim
        | valor     | Valor em decimal com ponto flutuante da transação     | Sim
        | dataHora  | Data/Hora no padrão ISO em que a transação acontecell | Sim

        Dica: O Spring Boot, por padrão, consegue compreender datas no padrão ISO sem problemas. 
        Tente utilizar um atributo do tipo OffsetDateTime ou LocalDateTime :)

    **Restrições e Domínio** 
        Uma transação aceita DEVE gerar uma resposta 201 Created sem nenhum corpo;
            Uma transação recusada DEVE gerar uma resposta 422 Unprocessable Entity sem nenhum corpo;
                
        Desafio/Extra: Ao invés de um corpo vazio, envie um JSON que informa quais erros de validação foram encontrados.
                Desafio/Extra: Uma requisição com um corpo JSON inválido gera uma resposta 400 Bad Request sem nenhum corpo.
            
            Toda transação DEVE ter valor e dataHora;
            Uma transação NÃO DEVE acontecer no futuro;
            Uma transação DEVE ter acontecido a qualquer momento no passado;
            Uma transação NÃO DEVE ter valor negativo;
            Uma transação DEVE ter valor igual ou maior que 0 (zero).

2. DELETE /transacao
    
    Esta requisição simplesmente apaga todos os dados de transação que estejam armazenados.

3. GET /estatistica

    Este endpoint deve retornar estatísticas das transações que aconteceram apenas nos últimos 60 segundos (1 minuto). Veja a seguir um exemplo de resposta esperado desse endpoint:

    {
        "count": 10,
        "sum": 1234.56,
        "avg": 123.456,
        "min": 12.34,
        "max": 123.56
    }

    Os campos no JSON acima significam o seguinte:

        | Campo     | Significado                                                   | Obrigatório?
        | ------    | ------                                                        |
        | count     | Quantidade de transações nos últimos 60 segundos              | Sim
        | sum       | Soma total do valor transactionado nos últimos 60 segundos    | Sim
        | avg       | Média do valor transacionado nos últimos 60 segundos          | Sim
        | min       | Menor valor transacionado nos últimos 60 segundos             | Sim
        | max       | Maior valor transacionado nos últimos 60 segundos             | Sim
        Dica: Há um objeto no Java 8+ chamado DoubleSummaryStatistics que pode lhe ajudar ou servir de inspiração ;)

**Restrições sob o Sistema/API**
	

    Este sistema NÃO DEVE utilizar nenhum outro sistema ou biblioteca de amazenamento de dados (por exemplo: H2, SQLite, MongoDB, etc.) ou Cache (Redis, Infinispan, Memcached, Ehcache, etc.);
    Esta API DEVE aceitar e responder apenas com JSON;
    Esta API DEVE ser Thread-safe. Isso signfica que todos os endpoints poderão estar sendo chamados a qualquer momento e em qualquer ordem e não devem quebrar por isso;
    Este sistema DEVE ter testes unitários (em coisas como Services, por exemplo);
    Esta API DEVE ter testes de integração;
    Lembre de usar uma arquitetura Hexagonal ou Clean Code ;)

**Extras**


    Esta sessão apresenta desafios extras para que você teste seus conhecimentos ao máximo, ou simplesmente queira fazer uma API com uma qualidade excepcional! São pontos desejáveis, mas não são obrigatórios.
        1. Testes para a Thread-safety do seu código: Você consegue testar se seu código realmente é thread-safe?
        2. Tratamento de erros: Você consegue tratar erros de validação e entrada inválida usando recursos do Spring Boot?
        3. Performance 1: Quantas transações por segundo você consegue processar? Como você calculou isso? Você consegue melhorar esse número?
        4. Performance 2: O endpoint GET /estatistica pode retornar em tempo constante (também conhecido como O(1)). Você consegue fazer isso?
        5. Modernização: Você consegue containerizar sua API?
        6. Observabilidade: Sua aplicação tem um endpoint de checagem de saúde da API (healthcheck)? Você publica métricas do sistema?
        7. Documentação: Sua aplicação está documentada? Há informações de como executá-la, como construí-la, etc.

